import "../styles/index.scss";
import $ from "jquery";
import "slick-carousel/slick/slick";
import "bootstrap/js/dist/modal";
import "bootstrap-datepicker";

$(document).ready(function () {
  $(".hamburger").click(function () {
    $(this).toggleClass("is-active");
    $(".wrapper").toggleClass("offcanvas--opened");
  });
  $(".offcanvas__navigation-link").click(function () {
    setTimeout(function () {
      $(".hamburger").removeClass("is-active");
      $(".wrapper").removeClass("offcanvas--opened");
    }, 500);
  });

  $("#toggleFilters").click(function () {
    $("#modalFilter").modal("show");
  });


  (function () {
    var allPanels = $("#accordion .collapse").hide();

    $("#accordion button").click(function () {
      allPanels.slideUp();
      $(this).parent().parent().next().slideDown();
      $(this).parent().next().addClass("test");
      console.log("upiren na botun");
      return false;
    });
  })();

  $(".slider--search").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    dots: true,
    autoplay: true,
  });
  $(".slider--featured").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    dots: false,
    autoplay: false,
    infinite: true,
    centerMode: true,
  });
  $(".slider--partners").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    arrows: false,
    duration: 200,
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    infinite: false,

    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          draggable: true,
        },
      },
    ],
  });
  $(".slider--hotel").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: ".slider--hotel-nav",
  });

  $(".slider--hotel-nav").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: ".slider--hotel",
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    arrows: false,
    infinite: false,
  });
});
